# ![Loot Master](https://gitlab.com/meditator/loot-master/raw/master/brand/title.png)

Made with [Godot Engine](https://godotengine.org). Loot Master is [free software](https://www.gnu.org/philosophy/free-sw.en.html). Code is licensed under the [GNU GPLv3](https://www.gnu.org/licenses/gpl-3.0-standalone.html). Art and sound is licensed under [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).

![Screenshot](https://gitlab.com/meditator/loot-master/raw/master/brand/shot1-small.png)

To modify this game, make sure Godot Engine is installed, then open project.godot.

### Special Thanks

To the authors of:

- [Blender](https://www.blender.org/)
- [GIMP](https://www.gimp.org/)
- [Audacity](https://www.audacityteam.org/)
