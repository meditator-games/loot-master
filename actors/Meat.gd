extends RigidBody

onready var area = Area.new()

func _ready():
	area.set_collision_mask_bit(0, true)
	area.add_child($CollisionShape.duplicate())
	area.connect("body_entered", self, "_body_entered")
	get_parent().call_deferred("add_child", area)

func _physics_process(delta):
	if area != null:
		area.translation = translation
	$MeshInstance.rotation.y += delta * PI * 0.5

func _body_entered(body):
	if body.has_method("give"):
		if body.give({"type": "food", "healing": randi() % 2 + 1}):
			GameState.play_audio_oneshot("Chew", self)
			queue_free()

func _exit_tree():
	if area != null:
		area.queue_free()
		area = null