extends RichTextLabel

const SPEED_PPS = 32

var lifetime = 0
var timer = 0
var world_position = Vector3()
var camera = null

func _enter_tree():
	lifetime = max(text.length() * (1.0/6.0), 1.0)

func _process(delta):
	timer += delta
	var screen_pos = camera.unproject_position(world_position) + Vector2(0, -32)
	rect_position = screen_pos - rect_size / 2.0
	rect_position.y -= timer * SPEED_PPS
	modulate = Color(1, 1, 1, 1.0 - timer / lifetime)
	if timer >= lifetime:
		queue_free()