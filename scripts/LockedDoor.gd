extends StaticBody

onready var GameState = get_node("/root/GameState")

func _ready():
	$Area.connect("body_entered", self, "_body_entered")

func _body_entered(body):
	if body.has_method("try_unlock"):
		if body.try_unlock(self):
			GameState.play_audio_oneshot("Door", self)