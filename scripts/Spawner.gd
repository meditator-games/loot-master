extends StaticBody

export (PackedScene) var actor_type
export (int) var max_spawns
export (float) var spawn_active_radius
export (float) var spawn_interval

onready var target_ref = weakref(get_node("../Player"))

var spawn_timer = 0

func _process(delta):
	var target = target_ref.get_ref()
	if target != null and translation.distance_to(target.translation) < spawn_active_radius:
		spawn_timer += delta
		while spawn_timer >= spawn_interval:
			_spawn()
			spawn_timer -= spawn_interval

const SPAWN_SPOTS = [Vector3(1, 0, 0), Vector3(0, 0, 1), Vector3(-1, 0, 0), Vector3(0, 0, -1)]

func _spawn():
	var space = get_world().direct_space_state
	var unblocked = []
	for offset in SPAWN_SPOTS:
		var spot = translation + offset * 2.0
		var result = space.intersect_ray(translation, spot, [self], 1 | 2 | 4 | 16 | 64 | 128)
		if not result.has("collider") or result["collider"] == null:
			print(result)
			unblocked.append(spot)
	if not unblocked.empty():
		var spot = unblocked[randi() % unblocked.size()]
		var actor = actor_type.instance()
		actor.translation = spot
		GameState.play_audio_oneshot("Spawn", actor)
		get_parent().add_child(actor)
		spawn_timer -= spawn_interval