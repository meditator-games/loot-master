extends Spatial

onready var GameState = get_node("/root/GameState")

const MOUSE_SCROLL_SPEED = 0.5

func _ready():
	$PauseOverlay.visible = false
	$PauseOverlay.modulate = Color(1, 1, 1, 0)
	toggle_menu(true)
	_resized()
	get_viewport().connect("size_changed", self, "_resized")
	
func toggle_menu(enabled):
	get_tree().paused = not enabled
	if enabled:
		Input.set_mouse_mode(Input.MOUSE_MODE_CONFINED)
		$PauseTween.interpolate_property($PauseOverlay, "modulate", null, Color(1, 1, 1, 0), 0.2, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
		$PauseTween.start()
	else:
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		$PauseTween.interpolate_property($PauseOverlay, "modulate", null, Color(1, 1, 1, 1), 0.2, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
		$PauseTween.start()

func _resized():
	var size = OS.get_window_size()
	$Camera.size = size.y / 32

func _process(delta):
	if GameState.mode == GameState.MODE_EDITOR:
		step_editor(delta)

func step_editor(delta):
	if Input.is_action_just_pressed("ui_cancel"):
		toggle_menu(get_tree().paused)
	
	if $PauseOverlay.modulate.a >= 1.0:
		$PauseOverlay.mouse_filter = Control.MOUSE_FILTER_STOP
	else:
		$PauseOverlay.mouse_filter = Control.MOUSE_FILTER_IGNORE
	$PauseOverlay.visible = $PauseOverlay.modulate.a > 0
		
	if Input.get_mouse_mode() == Input.MOUSE_MODE_CONFINED:
		var mouse_pos = get_viewport().get_mouse_position()
		var dim = OS.window_size
		if mouse_pos.x == 0 or Input.is_action_pressed("ui_left"):
			$Camera.translation.x -= MOUSE_SCROLL_SPEED
		if mouse_pos.x == dim.x - 1 or Input.is_action_pressed("ui_right"):
			$Camera.translation.x += MOUSE_SCROLL_SPEED
		if mouse_pos.y == 0 or Input.is_action_pressed("ui_up"):
			$Camera.translation.z -= MOUSE_SCROLL_SPEED
		if mouse_pos.y == dim.y - 1 or Input.is_action_pressed("ui_down"):
			$Camera.translation.z += MOUSE_SCROLL_SPEED

func _on_ResumeButton_pressed():
	toggle_menu(true)


func _on_RestartButton_pressed():
	pass # Replace with function body.


func _on_QuitButton_pressed():
	GameState.SoundButton.play()
	GameState.fade_to(GameState, "quit")

func _on_EditButton_pressed():
	pass # Replace with function body.
