extends Spatial

onready var GameState = get_node("/root/GameState")

const MOUSE_SCROLL_SPEED = 0.5

func _ready():
	$PauseOverlay.visible = false
	$PauseOverlay.modulate = Color(1, 1, 1, 0)
	_resized()
	toggle_menu(true)
	get_viewport().connect("size_changed", self, "_resized")
	
func toggle_menu(enabled):
	get_tree().paused = not enabled
	if enabled:
		Input.set_mouse_mode(Input.MOUSE_MODE_CONFINED)
		$PauseTween.interpolate_property($PauseOverlay, "modulate", null, Color(1, 1, 1, 0), 0.2, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
		$PauseTween.start()
	else:
		Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
		$PauseTween.interpolate_property($PauseOverlay, "modulate", null, Color(1, 1, 1, 1), 0.2, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
		$PauseTween.start()
		$Playfield/Player.reset_inputs()

func _resized():
	var size = OS.get_window_size()
	#$Playfield/Camera.size = size.y / 32

func _process(delta):
	if GameState.mode == GameState.MODE_GAME:
		step_game(delta)

func step_game(delta):
	if Input.is_action_just_pressed("ui_cancel"):
		if get_tree().paused or $Playfield/Player.hp > 0:
			toggle_menu(get_tree().paused)
	
	if $Playfield/Player != null:
		var player = $Playfield/Player
		$HUD/HP.text = "HP: " + str(player.hp) + "/" + str(player.max_hp)
		$HUD/EXP.text = "EXP: " + str(player.xp) + "/" + str(player.max_xp)
		if player.keys > 0:
			$HUD/EXP.text += "\nKeys: " + str(player.keys)
		$HUD/Coins.text = "$" + str(player.coins)
	
	if $PauseOverlay.modulate.a >= 1.0:
		$PauseOverlay.mouse_filter = Control.MOUSE_FILTER_STOP
	else:
		$PauseOverlay.mouse_filter = Control.MOUSE_FILTER_IGNORE
	$PauseOverlay.visible = $PauseOverlay.modulate.a > 0

func _on_ResumeButton_pressed():
	GameState.SoundButton.play()
	toggle_menu(true)

func _on_RestartButton_pressed():
	GameState.SoundButton.play()
	GameState.fade_to(GameState, "restart_level")

func _on_QuitButton_pressed():
	GameState.SoundButton.play()
	GameState.fade_to(GameState, "quit_to_title")

func _on_EditButton_pressed():
	GameState.SoundButton.play()

func _on_CheckpointB_body_entered(body):
	$Playfield/Player.spawn_point = $Playfield/SpawnB.translation

func _on_EndTrigger_body_entered(body):
	GameState.fade_duration = 1.0
	GameState.fade_to(GameState, "show_ending", [body])

func _on_Tutorial1Trigger_body_entered(body):
	if $Playfield/Player.tutorialStep < 2:
		$Playfield/Player.tutorialStep = 2
		GameState.print_at($Playfield/Player, "I can click to swing my sword and break these barrels.")


func _on_Tutorial2Trigger2_body_entered(body):
	if $Playfield/Player.tutorialStep < 3:
		$Playfield/Player.tutorialStep = 3
		GameState.print_at($Playfield/Player, "I hope there are no monsters here.")
