extends RigidBody

const SWING_DURATION = 0.2
const MAX_DIST = 1.9

var timer = 0.0
var user_ref = null

func get_user():
	if user_ref != null and user_ref.get_ref() != null:
		return user_ref.get_ref()
	return null

func _ready():
	connect("body_entered", self, "_body_entered")
	angular_velocity.y = (PI * 0.5) * (1.0/SWING_DURATION)
	
func _body_entered(body):
	if body is GridMap:
		GameState.play_audio_oneshot("SwordClang", self)
		#damage tile?
		#var gpos = result.collider.world_to_map(result.position)
		#result.collider.get_cell_item(gpos.x, gpos.y, gpos.z)
		#result.collider.set_cell_item(gpos.x, gpos.y, gpos.z, -1)
		return
	var user = get_user()
	if body.has_method("damage") and user != body:
		GameState.play_audio_oneshot("Thud", body)
		var xp = body.damage(randi() % 2 + 1, "slashing", -linear_velocity)
		if xp > 0 and user != null:
			user.grant_xp(xp)
		return
	
	GameState.play_audio_oneshot("SwordClang", self)

func _destroy():
	visible = false
	$CollisionShape.disabled = true
	var user = get_user()
	if user != null:
		user.sword = null
	queue_free()

func _physics_process(delta):
	timer += delta
	var user = get_user()
	if timer > SWING_DURATION and (user == null \
			or not Input.is_action_pressed("command_1") \
			or user.translation.distance_to(translation) >= MAX_DIST):
		_destroy()
		