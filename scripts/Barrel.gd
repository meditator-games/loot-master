extends RigidBody

const Plywood = preload("res://actors/Plywood.tscn")
const Coin = preload("res://actors/Coin.tscn")
const Meat = preload("res://actors/Meat.tscn")

func damage(val, type, push):
	var debris_count = randi() % 3 + 3
	for i in range(debris_count):
		var plywood = Plywood.instance()
		plywood.linear_velocity = Vector3(randf() * 2.0 - 1.0, randf(), randf() * 2.0 - 1.0)* 2.0 + push * 2.0
		plywood.angular_velocity = Vector3(randf() * 2.0 - 1.0, randf() * 2.0 - 1.0, randf() * 2.0 - 1.0) * 4.0
		plywood.translation = translation 
		get_parent().add_child(plywood)
	var roll = randi() % 10
	var spawned = []
	if roll == 0:
		spawned.append(Meat.instance())
	elif roll < 5:
		spawned.append(Coin.instance())
	for obj in spawned:
		get_parent().add_child(obj)
		obj.translation = translation
	GameState.play_audio_oneshot("Barrel", self)
	queue_free()
	return 0