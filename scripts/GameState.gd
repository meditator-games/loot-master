extends Node

const Cursor = preload("res://ui/mouse-cursor.png")
const FloatingText = preload("res://ui/FloatingText.tscn")
const FadeOverlay = preload("res://ui/FadeOverlay.tscn")
const TitleScreen = preload("res://scenes/Title.tscn")

onready var SoundButton = $Sounds/Button
var Game = null
var Ending = null
var final_time = 0
var final_coins = 0
var final_max_hp = 0

const GRAV = -24.8
const DEFAULT_FADE_DURATION = 0.2

enum { MODE_TITLE, MODE_EDITOR, MODE_GAME, MODE_ENDING }

var mode = MODE_TITLE
var fade_cb = null
var fade_overlay = null
var last_overlay_alpha = 1
var current_scene = null
var fade_duration = DEFAULT_FADE_DURATION

func _ready():
	Input.set_custom_mouse_cursor(Cursor, Input.CURSOR_ARROW, Vector2(4, 6))
	_start_fade(false)

func _start_fade(on):
	var color = Color(1, 1, 1, 0)
	if on:
		color.a = 1
	$Tween.stop_all()
	$Tween.remove_all()
	$Tween.interpolate_property(fade_overlay, "modulate", null, color, fade_duration, Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
	$Tween.start()

func fade_to(obj, method, args=[]):
	fade_cb = [obj, method, args]
	fade_overlay.visible = true
	_start_fade(true)

func _process(delta):
	last_overlay_alpha = fade_overlay.modulate.a
	if not $Tween.is_active():
		if fade_cb != null:
			fade_cb[0].callv(fade_cb[1], fade_cb[2])
			fade_cb = null
		elif fade_overlay.modulate.a > 0:
			_start_fade(false)
	if $Tween.tell() == 0:
		fade_overlay.visible = fade_overlay.modulate.a > 0

func _enter_tree():
	var root = get_tree().root
	current_scene = root.get_child(root.get_child_count() -1)
	_recreate_fade_overlay()

func _recreate_fade_overlay():
	fade_overlay = FadeOverlay.instance()
	fade_overlay.modulate.a = last_overlay_alpha
	fade_overlay.name = "FadeOverlay"
	get_tree().root.get_children().back().add_child(fade_overlay)
	
func print_at(spatial, text):
	var camera = get_node("/root/Root/Playfield/Camera")
	var hud = get_node("/root/Root/HUD")
	if camera != null and hud != null:
		var label = FloatingText.instance()
		label.camera = camera
		label.world_position = spatial.translation
		label.bbcode_text = "[center]" + text + "[/center]"
		hud.add_child(label)
		
func play_audio_oneshot(name, at):
	var sound = get_node("Sounds/" + name).duplicate()
	sound.transform = at.global_transform
	get_tree().root.add_child(sound)
	sound.play()
	sound.connect("finished", sound, "queue_free")

func new_game():
	fade_duration = DEFAULT_FADE_DURATION
	mode = MODE_GAME
	if Game == null:
		var loader = ResourceLoader.load_interactive("res://scenes/Game.tscn")
		loader.wait()
		Game = loader.get_resource()
	_goto_scene(Game)

func restart_level():
	new_game()
	
func show_ending(player):
	mode = MODE_ENDING
	if Ending == null:
		var loader = ResourceLoader.load_interactive("res://scenes/Ending.tscn")
		loader.wait()
		Ending = loader.get_resource()
	final_time = player.play_time
	final_coins = player.coins
	final_max_hp = player.max_hp
	_goto_scene(Ending)
	
func quit_to_title():
	fade_duration = DEFAULT_FADE_DURATION
	mode = MODE_TITLE
	get_tree().paused = false
	_goto_scene(TitleScreen)
	
func quit():
	get_tree().quit()
	
func _goto_scene(scene):
	current_scene.free()
	current_scene = scene.instance()
	get_tree().root.add_child(current_scene)
	get_tree().set_current_scene(current_scene)
	_recreate_fade_overlay()
