extends RigidBody

const Coin = preload("res://actors/Coin.tscn")
const Meat = preload("res://actors/Meat.tscn")

const MOVE_SPEED = 8.0
const ACCEL = 10.0
const SPAWN_DURATION = 0.8
export (float) var sight_distance = 16.0
onready var GameState = get_node("/root/GameState")
onready var target_ref = weakref(get_node("../Player"))
var target_pos = null
var vel = Vector3()
var damaged = {}
onready var hp = randi() % 2 + 1
var death_timer = 0
var spawn_timer = 0

func _ready():
	$MeshInstance.scale = Vector3()
	

func _process(delta):
	if hp <= 0:
		death_timer += delta
		if death_timer > 0.5:
			visible = not visible
		if death_timer > 1:
			var roll = randi() % 8
			var spawned = []
			if roll == 1:
				spawned.append(Meat.instance())
			elif roll > 1:
				spawned.append(Coin.instance())
			for obj in spawned:
				get_parent().add_child(obj)
				obj.translation = translation
			queue_free()

func _physics_process(delta):
	spawn_timer = min(spawn_timer + delta, SPAWN_DURATION)
	if spawn_timer < SPAWN_DURATION:
		$MeshInstance.scale = Vector3(1, 1, 1) * (spawn_timer / SPAWN_DURATION)
		return
	for body in damaged.keys():
		damaged[body] -= delta
		if damaged[body] < 0:
				damaged.erase(body)
				
	if hp <= 0:
		return
		
	var moving = false
	var chasing = false
	var hvel = Vector3()
	var target = target_ref.get_ref()
	
	if target != null and translation.distance_to(target.translation) < sight_distance:
		var space = get_world().direct_space_state
		var result = space.intersect_ray(translation, target.translation, [self], 1 | 2 | 16 | 64)
		#var to_player = target.translation - translation
		#$RayCast.cast_to = transform.basis.xform_inv(to_player)
		#$RayCast.force_raycast_update()
		#var collider = $RayCast.get_collider()
		var collider = result["collider"] if result.has("collider") else null
		if collider != null and collider == target:
			target_pos = target.translation
			chasing = true
		if target_pos != null and (chasing or (target_pos - translation).length() > MOVE_SPEED * delta * delta * 8.0):
			var look_target = transform.basis.xform_inv(target_pos - translation)
			look_target.y = translation.y
			var transform_target = $MeshInstance.transform.looking_at(look_target, Vector3(0, 1, 0))
			$MeshInstance.transform = $MeshInstance.transform.interpolate_with(transform_target, 0.1)
			#var vel = (target_pos - translation).normalized() * MOVE_SPEED * delta
			hvel = -$MeshInstance.transform.basis.z.normalized() * MOVE_SPEED * delta
			moving = true
	
	if not moving:
		hvel = vel.linear_interpolate(Vector3(), delta * ACCEL)
	
	var vel_target = hvel.normalized() * MOVE_SPEED
	var diff = (vel_target - linear_velocity) * ACCEL
	diff.y = 0
	if not chasing or diff.dot(linear_velocity) >= 0:
		add_central_force(diff * mass)
	var pos = get_node("../Camera").unproject_position(translation) + Vector2(0, -64)
			
func damage(val, type, push):
	var xp = 0
	hp -= val
	if val > 0:
		GameState.print_at(self, "[color=#ffffff]" + str(val) + "[/color]")
	if hp <= 0 and mode != RigidBody.MODE_RIGID:
		mode = RigidBody.MODE_RIGID
		get_node("CollisionRay").free()
		collision_layer = 0
		xp = 1
		apply_central_impulse(push * 8.0)
	else:
		apply_central_impulse(push * 16.0)
	return xp
	
func _on_Imp_body_entered(body):
	if body.get_script() == get_script():
		apply_central_impulse((translation - body.translation) * 4.0)
		body.apply_central_impulse((body.translation - translation) * 4.0)
	elif body.has_method("damage") and not damaged.has(body) and hp > 0:
		GameState.play_audio_oneshot("Thud", body)
		body.damage(randi() % 2 + 1, "physical", linear_velocity)
		damaged[body] = 1.0/60.0 * 6.0
