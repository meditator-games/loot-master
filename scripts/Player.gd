extends RigidBody

const Sword = preload("res://actors/Sword.tscn")
const Coin = preload("res://scripts/Coin.gd")

const TUTORIAL_START = 5.0
const MAX_SPEED = 12.0
const JUMP_SPEED = 9
const ACCEL = 12
const DEACCEL = 8
const MAX_SLOPE_ANGLE = 40
const DEATH_PLANE_Y = -1
onready var Camera = get_node("../Camera")
onready var Spawn = get_node("../SpawnA")
onready var GameState = get_node("/root/GameState")
onready var spawn_point = Spawn.translation
var play_time = 0
var spawn_transform
var frozen = 0
var sword = null
var xmove = []
var zmove = []
var since_on_floor = 0
var max_hp = randi() % 3 + 3
var hp = max_hp
var xp = 0
var max_xp = 8
var coins = 0
var keys = 0
var respawn_phase = -1
var tutorialTimer = 0
var tutorialStep = 0
 

func _ready():
	_respawn()
	
func _respawn():
	mode = RigidBody.MODE_STATIC
	frozen = GameState.fade_duration
	translation = spawn_point
	linear_velocity = Vector3()
	reset_inputs()
	Camera.translation.x = spawn_point.x
	Camera.translation.z = spawn_point.z + 4.0
	respawn_phase = 1

func reset_inputs():
	xmove = []
	zmove = []

func sword_position(t):
	var sword_transform = transform
	sword_transform.basis = sword.transform.basis.rotated(Vector3(0, 1, 0), t)
	return sword_transform
	
func _input(event):
	if event is InputEventMouseMotion and event.button_mask & BUTTON_MASK_LEFT and sword != null:
		var force = Vector3(event.relative.x, 0, event.relative.y)
		if force.dot(sword.linear_velocity) > 0:
			var force_strength = force.length()
			if force_strength > 0.25:
				force = force * (0.25 / force_strength)
			sword.add_central_force(force)

func give(item):
	if item["type"] == "coins":
		coins += item["count"]
		GameState.print_at(self, "[color=#ffff00]+" + str(item["count"]) +"[/color]")
		return true
	elif item["type"] == "key":
		keys += 1
		GameState.print_at(self, "[color=#0000ff]+Key[/color]")
		return true
	elif item["type"] == "food":
		if hp == max_hp:
			return false
		GameState.print_at(self, "[color=#ff00ff]+" + str(item["healing"]) +"HP[/color]")
		hp += item["healing"]
		hp = min(hp, max_hp)
		return true

func try_unlock(door):
	if keys > 0:
		keys -= 1
		door.queue_free()
		GameState.print_at(self, "[color=#00ffff]Used Key[/color]")
		return true
	else:
		GameState.print_at(self, "It's Locked.")
	return false

func grant_xp(val):
	xp += val
	var hp_granted = 0
	while xp >= max_xp:
		xp -= max_xp
		max_xp *= 2
		max_hp += 1
		hp += 1
		hp_granted += 1
	if hp_granted > 0:
		GameState.print_at(self, "[color=#ff00ff]Level Up! +" + str(hp_granted)  + " Max HP[/color]")
	elif val > 0:
		GameState.print_at(self, "[color=#ff00ff]+" + str(val) + " XP[/color]")
		

func normalize_rad(r):
	while r <= -PI:
		r += PI
	while r > PI:
		r -= PI
	return r

func _physics_process(delta):
	if GameState.mode == GameState.MODE_GAME:
		play_time += delta
	if respawn_phase == 1:
		respawn_phase = -1
		mode = RigidBody.MODE_CHARACTER
		return
			
	var dir = Vector3()
	if tutorialStep < 1:
		tutorialTimer = min(tutorialTimer + delta, TUTORIAL_START)
		if tutorialTimer >= TUTORIAL_START:
			GameState.print_at(self, "Looks like I can press the \"WASD Keys\" to move...")
			tutorialStep = 1

	if frozen > 0:
		frozen = max(frozen - delta, 0)
	elif GameState.mode == GameState.MODE_GAME and hp > 0:
		if Input.is_action_just_pressed("move_north"):
			zmove.append(-1)
		if Input.is_action_just_pressed("move_south"):
			zmove.append(1)
		if Input.is_action_just_pressed("move_west"):
			xmove.append(-1)
		if Input.is_action_just_pressed("move_east"):
			xmove.append(1)
		if Input.is_action_just_released("move_north"):
			zmove.erase(-1)
		if Input.is_action_just_released("move_south"):
			zmove.erase(1)
		if Input.is_action_just_released("move_west"):
			xmove.erase(-1)
		if Input.is_action_just_released("move_east"):
			xmove.erase(1)
		if Input.is_action_just_pressed("command_1") or (sword != null and Input.is_action_pressed("command_1")):
			var mouse_pos = get_viewport().get_mouse_position()
			var cam_normal = Camera.project_ray_normal(mouse_pos)
			var cam_point = Camera.project_ray_origin(mouse_pos)
			var target = cam_point + cam_normal * ((cam_point.y - translation.y) / -cam_normal.y)
			if  sword == null:
				sword = Sword.instance()
				GameState.play_audio_oneshot("SwordWhoosh", sword)
				sword.user_ref = weakref(self)
				var offset = target - translation
				sword.transform = sword_position(atan2(-offset.z, offset.x) + PI * -0.25)
				sword.linear_velocity = linear_velocity
				get_parent().add_child(sword)
			else:
				var start_offset = sword.translation - translation
				var offset = target - translation
				var start_angle = atan2(-start_offset.z, start_offset.x)
				var angle = atan2(-offset.z, offset.x)
				var a_vel = min(0.1, normalize_rad(angle - start_angle))
				var diff_vel = a_vel - sword.angular_velocity.y
				sword.add_torque(Vector3(0, diff_vel, 0))

	if !xmove.empty():
		dir += Vector3(1, 0, 0) * xmove[0]
	if !zmove.empty():
		dir += Vector3(0, 0, 1) * zmove[0]
	
	var target = dir.normalized() * MAX_SPEED
	var diff = target - linear_velocity
	if target.length_squared() > 0:
		diff *= ACCEL
	else:
		diff *= DEACCEL
	
	if respawn_phase == -1:
		add_central_force(diff * mass)
	
	Camera.translation.x = lerp(Camera.translation.x, translation.x, 0.1)
	Camera.translation.y = lerp(Camera.translation.y, translation.y + 16.0, 0.1)
	Camera.translation.z = lerp(Camera.translation.z, translation.z + 6.0, 0.1)
	
	if sword != null:
		var force = (translation - sword.translation)
		force.y = 0
		sword.add_central_force(force)
		
	if translation.y < DEATH_PLANE_Y and respawn_phase == -1 and hp > 0:
		damage(1, "falling", Vector3())
		if hp > 0:
			respawn_phase = 0
			GameState.fade_to(self, "_respawn")

func damage(val, type, push):
	hp -= val
	if hp <= 0 and mode != RigidBody.MODE_RIGID:
		GameState.print_at(self, "[color=#ff0000]RIP[/color]")
		mode = RigidBody.MODE_RIGID
		GameState.fade_duration = 5.0
		GameState.fade_to(GameState, "restart_level")
		$CollisionShape2.queue_free()
		apply_impulse(Vector3(rand_range(-1, 1), rand_range(-1, 1), rand_range(-1, 1)) * 0.1, push.normalized() * 160.0)
	else:
		GameState.print_at(self, "[color=#ff0000]" + str(val) + "[/color]")
		apply_central_impulse(push.normalized() * 160.0)