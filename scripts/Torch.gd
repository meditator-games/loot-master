extends StaticBody

export (float) var light_range = 5

func _ready():
	$Light.omni_range = light_range

func _process(delta):
	$Light.light_energy = 0.9 + randf() * 0.1
