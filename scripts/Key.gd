extends RigidBody

onready var area = Area.new()
var death_timer = 0

func _ready():
	area.set_collision_mask_bit(0, true)
	area.add_child($CollisionShape.duplicate())
	area.connect("body_entered", self, "_body_entered")
	get_parent().call_deferred("add_child", area)

func _physics_process(delta):
	if area != null:
		area.translation = translation
	$MeshContainer/MeshInstance.rotation.x += delta * PI * 0.5
	if $Particles.emitting:
		death_timer += delta
	if death_timer >= $Particles.lifetime:
		queue_free()

func _body_entered(body):
	print("entered")
	if body.has_method("give"):
		GameState.play_audio_oneshot("Key", self)
		body.give({"type": "key"})
		$Particles.emitting = true
		$Particles.restart()
		$Particles.transform.basis = $MeshContainer/MeshInstance.transform.basis
		$MeshContainer.visible = false
		mode = RigidBody.MODE_STATIC
		collision_mask = 0
		area.queue_free()
		area = null

func _exit_tree():
	if area != null:
		area.queue_free()
		area = null