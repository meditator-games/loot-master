extends Spatial

onready var GameState = get_node("/root/GameState")

var timer = 0
onready var start_basis = $Camera.transform.basis


func _on_NewButton_pressed():
	GameState.SoundButton.play()
	GameState.fade_to(GameState, "new_game")
	
func _on_QuitButton_pressed():
	GameState.SoundButton.play()
	GameState.fade_to(GameState, "quit")

func _physics_process(delta):
	timer += delta
	$Camera.transform.basis = start_basis.rotated(Vector3(0, 1, 0), sin(timer) * 0.01).rotated(Vector3(0, 0, 1), sin(timer * 0.625194) * 0.01)