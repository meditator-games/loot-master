extends Spatial

onready var GameState = get_node("/root/GameState")

func format_duration(secs):
	return str(floor(secs / 60.0)) + ":" + str(int(round(secs)) % 60)

func _ready():
	$Overlay/FinalScore.text =  "Max HP: " + str(GameState.final_max_hp) + \
						"\nSilver Collected: " + str(GameState.final_coins) + \
						"\nPlay Time: " + format_duration(GameState.final_time)

func _on_ReturnButton_pressed():
	GameState.fade_to(GameState, "quit_to_title")

func _physics_process(delta):
	$Backdrop.rotation.y += delta * PI * (1.0/16.0)